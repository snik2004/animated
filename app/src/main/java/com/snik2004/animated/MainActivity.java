package com.snik2004.animated;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageSwitcher;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    ImageView imageViewLol;
    ImageView imageViewFun;
    private boolean isImageVisible = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imageViewLol = findViewById(R.id.image_lol);
        imageViewFun = findViewById(R.id.image_fun);
        imageViewLol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeToFun();
            }
        });
        imageViewFun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeToFun();
            }
        });
    }
    public void changeToFun() {
        if (isImageVisible) {
            imageViewLol.animate().alpha(0).scaleX(0.1f).scaleY(0.1f).rotation(3600).setDuration(3000);
            imageViewFun.animate().alpha(1).scaleX(1f).scaleY(1f).rotation(3600).setDuration(3000);
            isImageVisible=false;
        }else {
            imageViewFun.animate().alpha(0).scaleX(0.1f).scaleY(0.1f).rotation(-3600).setDuration(3000);
            imageViewLol.animate().alpha(1).scaleX(1f).scaleY(1f).rotation(-3600).setDuration(3000);
            isImageVisible=true;
        }

    }
}
